﻿#include <iostream>

int ScrabbleScore(std::string word)
{
	int x = 0;
	int index = 0;
	
	while (word[index])
	{
		char letter = toupper(word[index]);  //Zwraca znak zamieniony z dużej litery na małą
		if (letter == 'A' || letter == 'E' || letter == 'I' || letter == 'O' || letter == 'U' || letter == 'L' || letter == 'N' || letter == 'R' || letter == 'S' || letter == 'T')
			x += 1;
		else if (letter == 'D' || letter == 'G')
			x += 2;
		else if (letter == 'B' || letter == 'C' || letter == 'M' || letter == 'P')
			x += 3;
		else if (letter == 'F' || letter == 'H' || letter == 'V' || letter == 'W' || letter == 'Y')
			x += 4;
		else if (letter == 'K')
			x += 5;
		else if (letter == 'J' || letter == 'X')
			x += 8;
		else if (letter == 'Q' || letter == 'Z')
			x += 10;
		++index;
	}
	return x;
}

int main()
{
	std::string word;
	std::cout << "Wprowadz slowo: ";
	std::cin >> word;
	std::cout << "Wynik dla '" << word << "' to " << ScrabbleScore(word) << std::endl;
	
	return 0;
}